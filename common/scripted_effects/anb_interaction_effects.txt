﻿grant_title_tyranny_effect = {
	if = {
		limit = {
			title_grant_is_tyrannical_trigger = yes
		}
		add_tyranny = revoke_title_tyranny_gain
	}
}