﻿# Anbennar - a bunch of our icons added here (not all probably), some bulwari specific charge lists

colored_emblem_texture_lists = {

	charge = {
		10 	= "ce_anchor.dds"
		1 	= "ce_ant.dds"
		10 	= "ce_arm_holding_sword.dds"
		10 	= "ce_axe.dds"
		1 	= "ce_bat.dds"
		10 	= "ce_bear_passant.dds"
		10 	= "ce_bear_rampant.dds"
		1 	= "ce_bee.dds"
		10 	= "ce_boar_head.dds"
		10 	= "ce_boar_passant.dds"
		10 	= "ce_bow.dds"
		1 	= "ce_breeches.dds"
		10 	= "ce_bull_head.dds"
		10 	= "ce_bull_passant.dds"
		10 	= "ce_bull_rampant.dds"
		10 	= "ce_caltrop.dds"
		10 	= "ce_carbuncle.dds"
		1 	= "ce_chains_no_border.dds"
		1 	= "ce_chains.dds"
		10 	= "ce_chalice.dds"
		10 	= "ce_cinquefoil_01.dds"
		10 	= "ce_cinquefoil_02.dds"
		10 	= "ce_cinquefoil.dds"
		1 	= "ce_circle.dds"
		10 	= "ce_circle.dds"
		10 	= "ce_circles_01.dds"
		10 	= "ce_clam.dds"
		1 	= "ce_clothes.dds"
		1 	= "ce_cockerel.dds"
		#1 	= "ce_crown_lombard.dds"
		0 	= "ce_desdichado.dds"
		10 	= "ce_diamond_01.dds"
		10 	= "ce_dolphin.dds"
		#10 	= "ce_dragon.dds"
		1 	= "ce_duck.dds"
		20 	= "ce_eagle_crown.dds"
		10 	= "ce_eagle_double.dds"
		10 	= "ce_eagle_head.dds"
		100 = "ce_eagle.dds"
		10 	= "ce_ermine_spot.dds"
		1 	= "ce_fire.dds"
		10 	= "ce_fish.dds"
		10 	= "ce_fleece.dds"
		2 	= "ce_fleur_florence.dds"
		10 	= "ce_fleur.dds"
		50 	= "ce_flower.dds"
		1 	= "ce_frog.dds"
		10 	= "ce_goat.dds"
		20 	= "ce_grain.dds"
		10 	= "ce_griffin_head.dds"
		10 	= "ce_griffin_rampant.dds"
		1 	= "ce_hamades_03.dds"
		10 	= "ce_hand.dds"
		0 	= "ce_head.dds"
		10 	= "ce_heart.dds"
		1 	= "ce_hedgehog.dds"
		1 	= "ce_hive.dds"
		10 	= "ce_horn.dds"
		10 	= "ce_horse_rampant.dds"
		10 	= "ce_horse_shoe.dds"
		10 	= "ce_horse_statant.dds"
		1 	= "ce_hound.dds"
		1 	= "ce_key.dds"
		#1 	= "ce_keys.dds"
		10 	= "ce_knight.dds"
		10 	= "ce_laurels.dds"
		10 	= "ce_lion_head.dds"
		50 	= "ce_lion_passant_guardant.dds"
		1 	= "ce_lion_rampant_axe.dds"
		5 	= "ce_lion_rampant_crown_forked.dds"
		1 	= "ce_lion_rampant_crown_sword.dds"
		10 	= "ce_lion_rampant_crown.dds"
		10 	= "ce_lion_rampant_forked.dds"
		10 	= "ce_lion_rampant_regardant.dds"
		50 	= "ce_lion_rampant.dds"
		10 	= "ce_lobster.dds"
		20 	= "ce_martlet.dds"
		10 	= "ce_mascle_01.dds"
		10 	= "ce_mascle.dds"
		10 	= "ce_moon.dds"
		10 	= "ce_nettle_leaf.dds"
		10 	= "ce_owl.dds"
		10 	= "ce_panther.dds"
		0 	= "ce_paradox.dds"
		10 	= "ce_pomegranate.dds"
		10 	= "ce_portcullis.dds"
		10 	= "ce_pot.dds"
		10 	= "ce_rook.dds"
		0 	= "ce_sabres.dds"
		10 	= "ce_scales.dds"
		1 	= "ce_shamrock.dds"
		10 	= "ce_sheep.dds"
		10 	= "ce_shield_portugal.dds"
		10 	= "ce_snake.dds"
		10 	= "ce_spearhead.dds"
		10 	= "ce_squirrel.dds"
		10 	= "ce_stag.dds"
		10 	= "ce_star_05_pierced.dds"
		10 	= "ce_star_05.dds"
		10 	= "ce_star_06_pierced.dds"
		10 	= "ce_star_06.dds"
		10 	= "ce_star_08.dds"
		5 	= "ce_star_16.dds"
		10 	= "ce_star_hexagram.dds"
		10 	= "ce_sun.dds"
		1 	= "ce_swan.dds"
		20 	= "ce_tower.dds"
		10 	= "ce_tree.dds"
		1 	= "ce_unicorn.dds"
		10 	= "ce_waterlily.dds"
		10 	= "ce_wheel.dds"
		10 	= "ce_wildman.dds"
		10 	= "ce_wings.dds"
		10 	= "ce_wolf_head.dds"
		10 	= "ce_wolf.dds"
		10 	= "ce_wyvern_rising.dds"
		20 	= "ce_wyvern.dds"
		20 	= "ce_lion_guardant.dds"
		20 	= "ce_lion_passant.dds"
		20 	= "ce_waterlily.dds"
		1 	= "ce_waterlily_angria.dds"
		1 	= "ce_barbels_addorsed.dds"
		1 	= "ce_salms_addorsed.dds"
		10 	= "ce_lozenge.dds"
		10 	= "ce_lion_passant_small.dds"
		1 	= "ce_hound_rampant.dds"
		1 	= "ce_escarbuncle_flory.dds"
		1 	= "ce_bear_head.dds"

		#Anbennar Additions to basic charge list

		10	= "ce_anb_anvil.dds"
		10	= "ce_anb_apple.dds"
		10	= "ce_anb_bear_head.dds"
		10	= "ce_anb_book.dds"
		1	= "ce_anb_cog.dds"
		1	= "ce_anb_crown.dds"
		1	= "ce_anb_crystal.dds"
		10	= "ce_anb_dragon_rampant.dds"
		10	= "ce_anb_elvenized_moon.dds"
		1	= "ce_anb_elvenized_rose.dds"
		10	= "ce_anb_grain.dds"
		1	= "ce_anb_grapes.dds"
		10	= "ce_anb_heart.dds"
		10	= "ce_anb_old_damerian_moon.dds"
		1	= "ce_anb_pear.dds"
		10	= "ce_anb_raven.dds"
		1	= "ce_anb_sea_lion_rampant.dds"
		1	= "ce_anb_ship.dds"
		10	= "ce_anb_stag_rampant.dds"
		1	= "ce_anb_sword.dds"
		1	= "ce_anb_sword_02.dds"
		10	= "ce_anb_wolf_regardant.dds"
		1	= "ce_anb_woman.dds"
	}
	

	#################################################
	################### Anbennar ####################
	#################################################

	# Bulwar

	bulwari_charge_list = {
		#3000 = "ce_anb_flower_01.dds"
		#3000 = "ce_anb_flower_02.dds"
		3000 = "ce_anb_lamp.dds"
		3000 = "ce_anb_sun_01.dds"
		3000 = "ce_anb_sun_02.dds"
		3000 = "ce_anb_sun_03.dds"
		3000 = "ce_anb_sun_04.dds"
		3000 = "ce_anb_sun_05.dds"
		500 = "ce_anb_sun_08.dds"
		500 = "ce_anb_sun_08_12.dds"
		500 = "ce_anb_sun_09.dds"
		500 = "ce_anb_sun_09_12.dds"
		3000 = "ce_eastern_sun.dds"
		1000 = "ce_lotus.dds"
		1000 = "ce_lotus_big.dds"
		3000 = "ce_mena_sun.dds"
		1000 = "ce_rising_sun.dds"
		1000 = "ce_solomon_star_08.dds"
		1000 = "ce_star_06.dds"
		1000 = "ce_star_08.dds"
		2000 = "ce_star_16.dds"
		3000 = "ce_star_hexagram.dds"
		3000 = "ce_star_ishtar.dds"
		3000 = "ce_sun.dds"
		#3000 = "ce_sun_india.dds"
		4000 = "ce_mena_lion.dds"
		1000 = "ce_persian_sun_lion.dds"
		3000 = "ce_lion_passant_sword.dds"
		3000 = "ce_mena_ceremonial_sword.dds"
		3000 = "ce_mena_rosette.dds"
		3000 = "ce_mena_axe.dds"
		3000 = "ce_lotus_flower.dds"
		4000 = "ce_anb_bulwar_bull_01.dds"
		4000 = "ce_anb_bulwar_lion.dds"
	}

	bulwari_circle_charge_list = {
		#3000 = "ce_anb_flower_01.dds"
		#3000 = "ce_anb_flower_02.dds"
		10000 = "ce_anb_sun_01.dds"
		10000 = "ce_anb_sun_02.dds"
		10000 = "ce_anb_sun_03.dds"
		10000 = "ce_anb_sun_04.dds"
		10000 = "ce_anb_sun_05.dds"
		500 = "ce_anb_sun_08.dds"
		500 = "ce_anb_sun_08_12.dds"
		500 = "ce_anb_sun_09.dds"
		500 = "ce_anb_sun_09_12.dds"
		3000 = "ce_eastern_sun.dds"
		1000 = "ce_lotus.dds"
		1000 = "ce_lotus_big.dds"
		3000 = "ce_mena_sun.dds"
		#3000 = "ce_sun_india.dds"
	}
	
	##################################################
	################### Base Game ####################
	##################################################

	religion_icons = {
		1 =  ce_star_08.dds # fallback

		#Anbennar
		special_selection = { trigger = { scope:faith = { has_icon = "the_dame" } } 10000000 = "ce_anb_the_dame.dds" }
		
		special_selection = { trigger = { scope:faith = { has_icon = "akanism" } } 10000000 = "ce_religion_akanism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "akanism_reformed" } } 10000000 = "ce_religion_akanism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "ari_buddhism" } } 10000000 = "ce_religion_buddhism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "armenian" } } 10000000 = "ce_religion_christianity_nestorian.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "ashari" } } 10000000 = "ce_religion_islam_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "azariqa" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "baltic" } } 10000000 = "ce_religion_pagan_romuva.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "baltic_custom" } } 10000000 = "ce_religion_pagan_romuva.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "baltic_reformed" } } 10000000 = "ce_religion_pagan_romuva.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "bon" } } 10000000 = "ce_religion_bon.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "buddhism" } } 10000000 = "ce_religion_buddhism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "buddhism_custom" } } 10000000 = "ce_religion_buddhism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "catholic" } } 10000000 = "ce_religion_christianity_catholic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "catholic_custom" } } 10000000 = "ce_religion_christianity_catholic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "christianity_Armenian" } } 10000000 = "ce_religion_armenian.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "christianity_bogomilist" } } 10000000 = "ce_religion_bogomilist.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "christianity_cathar" } } 10000000 = "ce_religion_cathar.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "christianity_Iconoclasm" } } 10000000 = "ce_religion_iconoclasm.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "christianity_lollard" } } 10000000 = "ce_religion_lollard.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "christianity_messalian" } } 10000000 = "ce_religion_messalian.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "christianity_paulicanism" } } 10000000 = "ce_religion_paulicanism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "christianity_waldensian" } } 10000000 = "ce_religion_waldensian.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "coptic" } } 10000000 = "ce_religion_christianity_nestorian.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "coptic_custom" } } 10000000 = "ce_religion_christianity_nestorian.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_1" } } 10000000 = "ce_religion_custom_faith_1.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_2" } } 10000000 = "ce_religion_custom_faith_2.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_3" } } 10000000 = "ce_religion_custom_faith_3.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_4" } } 10000000 = "ce_religion_islam_ismailism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_5" } } 10000000 = "ce_religion_custom_faith_5.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_6" } } 10000000 = "ce_religion_custom_faith_6.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_7" } } 10000000 = "ce_religion_custom_faith_7.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_8" } } 10000000 = "ce_religion_custom_faith_8.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_9" } } 10000000 = "ce_religion_custom_faith_9.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "custom_faith_10" } } 10000000 = "ce_religion_custom_faith_10.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "default" } } 10000000 = "ce_religion_pagan_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "default_custom" } } 10000000 = "ce_religion_pagan_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "donyipoloism" } } 10000000 = "ce_religion_pagan_zunist.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "dualism" } } 10000000 = "ce_religion_dualism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "exp_ari_buddhism" } } 10000000 = "ce_religion_buddhism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "finno_ugric" } } 10000000 = "ce_religion_pagan_suomenusko.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "finno_ugric_custom" } } 10000000 = "ce_religion_pagan_suomenusko.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "finno_ugric_reformed" } } 10000000 = "ce_religion_pagan_suomenusko.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "germanic" } } 10000000 = "ce_religion_pagan_germanic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "germanic_custom" } } 10000000 = "ce_religion_pagan_germanic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "germanic_reformed" } } 10000000 = "ce_religion_pagan_germanic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "ghulat" } } 10000000 = "ce_religion_islam_asharism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "hellenic" } } 10000000 = "ce_religion_pagan_hellenic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "hellenic_custom" } } 10000000 = "ce_religion_pagan_hellenic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "hellenic_reformed" } } 10000000 = "ce_religion_pagan_hellenic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "hinduism" } } 10000000 = "ce_religion_hinduism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "hinduism_custom" } } 10000000 = "ce_religion_hinduism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "iamaism_buddhism" } } 10000000 = "ce_religion_buddhism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "ibadi" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "ibadi_custom" } } 10000000 = "ce_religion_islam_asharism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "icon_religion_catholicism" } } 10000000 = "ce_religion_christianity_catholic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "imami" } } 10000000 = "ce_religion_islam_asharism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "islam_druze" } } 10000000 = "ce_religion_druze.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "jainism" } } 10000000 = "ce_religion_jain_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "jainism_custom" } } 10000000 = "ce_religion_jain_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "judaism" } } 10000000 = "ce_religion_judaism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "judaism_custom" } } 10000000 = "ce_religion_judaism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "kushism" } } 10000000 = "ce_religion_kushism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "kushism_reformed" } } 10000000 = "ce_religion_kushism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "lamaism_buddhism" } } 10000000 = "ce_religion_buddhism_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "manichean" } } 10000000 = "ce_religion_manichean.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "masmudi" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "maturidi" } } 10000000 = "ce_religion_islam_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "melieism" } } 10000000 = "ce_religion_melieism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "miaphysite" } } 10000000 = "ce_religion_christianity_nestorian.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "muhakkima" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "muhakkima_1" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "muhakkima_2" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "muhakkima_3" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "muhakkima_4" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "muhakkima_5" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "muhakkima_6" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "mutazila" } } 10000000 = "ce_religion_islam_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "muwalladi" } } 10000000 = "ce_religion_islam_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "nadjat" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "nestorian" } } 10000000 = "ce_religion_christianity_coptic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "nestorian_custom" } } 10000000 = "ce_religion_christianity_coptic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "orthodox" } } 10000000 = "ce_religion_christianity_orthodox.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "orthodox_custom" } } 10000000 = "ce_religion_christianity_orthodox.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "pagan" } } 10000000 = "ce_religion_pagan_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "pagan_default" } } 10000000 = "ce_religion_pagan_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "pagan_magyar" } } 10000000 = "ce_religion_magyar.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "pagan_reformed" } } 10000000 = "ce_religion_pagan_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "quranism" } } 10000000 = "ce_religion_islam_iibadi.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "rrmeaism" } } 10000000 = "ce_religion_rrmeaism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "sedism" } } 10000000 = "ce_religion_sedism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "shia" } } 10000000 = "ce_religion_islam_asharism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "shia_custom" } } 10000000 = "ce_religion_islam_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "slavic" } } 10000000 = "ce_religion_pagan_slavic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "slavic_custom" } } 10000000 = "ce_religion_pagan_slavic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "slavic_reformed" } } 10000000 = "ce_religion_pagan_slavic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "spirit_bon" } } 10000000 = "ce_religion_bon.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "sufri" } } 10000000 = "ce_religion_islam_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "sunni" } } 10000000 = "ce_religion_islam_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "sunni_custom" } } 10000000 = "ce_religion_islam_generic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "taoism" } } 10000000 = "ce_religion_taoism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "tengri" } } 10000000 = "ce_religion_pagan_tengri.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "tengri_custom" } } 10000000 = "ce_religion_pagan_tengri.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "tengri_reformed" } } 10000000 = "ce_religion_pagan_tengri.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "unselected" } } 10000000 = "ce_religion_christianity_catholic.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "mande" } } 10000000 = "ce_religion_vodun.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "waaqism" } } 10000000 = "ce_religion_waaqism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "waaqism_reformed" } } 10000000 = "ce_religion_waaqism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "west_african" } } 10000000 = "ce_religion_pagan_westafrican.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "west_african_bori" } } 10000000 = "ce_religion_west_african_bori.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "west_african_bori_reformed" } } 10000000 = "ce_religion_west_african_bori.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "west_african_custom" } } 10000000 = "ce_religion_pagan_westafrican.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "west_african_dogon" } } 10000000 = "ce_religion_west_african_bori.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "west_african_reformed" } } 10000000 = "ce_religion_pagan_westafrican.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "west_african_roog_sene" } } 10000000 = "ce_religion_african_roog_sene.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "west_african_roog_sene_reformed" } } 10000000 = "ce_religion_african_roog_sene.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "yazidism" } } 10000000 = "ce_religion_yazidism.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "yoruba" } } 10000000 = "ce_religion_yoruba.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "zoroastrian" } } 10000000 = "ce_religion_zoroastrianism_generic_faravahar.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "zoroastrian_custom" } } 10000000 = "ce_religion_zoroastrianism_generic_faravahar.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "zunist" } } 10000000 = "ce_religion_pagan_zunist.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "zunist_custom" } } 10000000 = "ce_religion_pagan_zunist.dds" }
		special_selection = { trigger = { scope:faith = { has_icon = "zunist_reformed" } } 10000000 = "ce_religion_pagan_zunist.dds" }
	}
}
