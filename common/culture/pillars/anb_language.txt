﻿language_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_common }
			multiply = 10
		}
	}
	
	color = { 222 222 222 }
}

language_damerian_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_damerian_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_damerian_common }
			multiply = 10
		}
	}
	
	color = { 28 124 140 }
}

language_alenic_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_alenic_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_alenic_common }
			multiply = 10
		}
	}
	
	color = { 48 51 108 }
}

language_small_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_small_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_small_common }
			multiply = 10
		}
	}
	
	color = { 255 127 39 }
}

language_lencori_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_lencori_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_lencori_common }
			multiply = 10
		}
	}
	
	color = { 142 64 58 }
}

language_vernid_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_vernid_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_vernid_common }
			multiply = 10
		}
	}
	
	color = { 150 0 0 }
}

language_milcori_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_milcori_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_milcori_common }
			multiply = 10
		}
	}
	
	color = { 91 135 192 }
}

language_businori_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_businori_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_businori_common }
			multiply = 10
		}
	}
	
	color = { 111 85 4 }
}

language_reachman_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_reachman_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_reachman_common }
			multiply = 10
		}
	}
	
	color = { 34 177 76 }
}

language_castanorian_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_castanorian_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_castanorian_common }
			multiply = 10
		}
	}
	
	color = { 222 222 222 }
}

language_rohibonic_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_rohibonic_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_rohibonic_common }
			multiply = 10
		}
	}
	
	color = { 239 228 176 }
}

language_borders_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_borders_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_borders_common }
			multiply = 10
		}
	}
	
	color = { 31 55 152 }
}

language_korbarid = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_korbarid
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_korbarid }
			multiply = 10
		}
	}
	
	color = { 52 52 52 }
}

language_cardesti = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_cardesti
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_cardesti }
			multiply = 10
		}
	}
	
	color = { 182 131 46 }
}

language_bulwari = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_bulwari
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_bulwari }
			multiply = 10
		}
	}
	
	color = { 222 222 0 }
}

language_barsuyitu = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_barsuyitu
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_barsuyitu }
			multiply = 10
		}
	}
	
	color = { 205 233 232 }
}

language_elumite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_elumite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_elumite }
			multiply = 10
		}
	}
	
	color = { 55 65 100 }
}

language_bahari = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_bahari
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_bahari }
			multiply = 10
		}
	}
	
	color = { 126 213 89 }
}

language_zanite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_zanite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_zanite }
			multiply = 10
		}
	}
	
	color = { 213 98 117 }
}

language_surani = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_surani
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_surani }
			multiply = 10
		}
	}
	
	color = { 245 207 137 }
}

language_masnsih = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_masnsih
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_masnsih }
			multiply = 10
		}
	}
	
	color = { 200 105 50 }
}

language_kheteratan = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_kheteratan
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_kheteratan }
			multiply = 10
		}
	}
	
	color = { 255 128 14 }
}

#a kheteratan dialect
language_ekhani = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_ekhani
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_ekhani }
			multiply = 10
		}
	}
	
	color = { 206 222 199 }
}

#a kheteratan dialect
language_deshaki = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_deshaki
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_deshaki }
			multiply = 10
		}
	}
	
	color = { 50 165 195 }
}

#a kheteratan dialect
language_khasani = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_khasani
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_khasani }
			multiply = 10
		}
	}
	
	color = { 90 135 65 }
}

language_gerudian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_gerudian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_gerudian }
			multiply = 10
		}
	}
	
	color = { 163 211 222 }
}

language_dwarven = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_dwarven
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_dwarven }
			multiply = 10
		}
	}
	
	color = { 128 128 128 }
}

language_cannorian_dwarven = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_cannorian_dwarven
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_cannorian_dwarven }
			multiply = 10
		}
	}
	
	color = { 128 128 128 }
}

language_segbandalic = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_segbandalic
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_segbandalic }
			multiply = 10
		}
	}
	
	color = { 36 96 143 }
}

#Should elven be divided? for now its not
language_elven = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_elven
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_elven }
			multiply = 10
		}
	}
	
	color = { 128 240 240 }
}

language_old_castanorian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_old_castanorian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_old_castanorian }
			multiply = 10
		}
	}
	
	color = { 254 254 254 }
}

language_old_lencori = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_old_lencori
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_old_lencori }
			multiply = 10
		}
	}
	
	color = { 206 0 60 }
}

language_gnomish = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_gnomish
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_gnomish }
			multiply = 10
		}
	}
	
	color = { 255 105 180 }
}

language_binwarji = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_binwarji
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_binwarji }
			multiply = 10
		}
	}
	
	color = { 233 233 233 }
}
