﻿# if you change thing, also change the modifier in anb_calindal_modifier for bladestewards wielding the blade
calindal_modifier = {
	monthly_prestige = 0.3
	monthly_dynasty_prestige = 0.04
	prowess = 11
	martial_per_prestige_level = 1
	learning_per_prestige_level = -1
	knight_effectiveness_mult = 0.10
}

# If you change this, also change the modifier on the artifact template for non Jaherian Cult dudes
dinatoldir_modifier = {
	monthly_prestige = 0.5
	monthly_dynasty_prestige = 0.04
	monthly_dynasty_prestige_mult = 0.05
	prowess = 10
	same_faith_opinion = 8
	levy_reinforcement_rate_same_faith = 0.20
	tolerance_advantage_mod = 8
}

ruby_crown_modifier = {
	monthly_prestige = 0.3
	monthly_dynasty_prestige = 0.04
	powerful_vassal_opinion = 8
	happy_powerful_vassal_tax_contribution_mult = 0.20
	happy_powerful_vassal_levy_contribution_mult = 0.20
}

crest_of_ishtara_modifier = {
}

elven_compass_modifier = {
	monthly_prestige = 0.15
	naval_movement_speed_mult = 0.25
}

pearl_of_the_dame_modifier = {
	monthly_dynasty_prestige = 0.04
	monthly_prestige = 1
	attraction_opinion = 5
	court_grandeur_baseline_add = 5
	naval_movement_speed_mult = 0.25
}

swansong_modifier = {
	monthly_prestige = 0.35
	court_grandeur_baseline_add = 2
	attraction_opinion = 20
	diplomacy_per_prestige_level = 1
	general_opinion = 5
	courting_scheme_power_mult = 0.5
}

iron_crown_of_gawed_modifier = {
	monthly_prestige = 0.25
	monthly_dynasty_prestige = 0.04
	vassal_limit = 5
	vassal_levy_contribution_mult = 0.05
	monthly_martial_lifestyle_xp_gain_mult = 0.1
}

silverbound_axe_of_dameria_modifier = {
	monthly_prestige = 0.25
	monthly_dynasty_prestige = 0.05
	court_grandeur_baseline_add = 4
}

ulfric_lance_modifier = {
	monthly_prestige = 0.4
	monthly_dynasty_prestige = 0.05
	heavy_cavalry_damage_mult = 0.1
	heavy_cavalry_toughness_mult = 0.1
}

carillon_of_the_lunetein_modifier = {
	monthly_dynasty_prestige = 0.05
	monthly_prestige = 0.6
	monthly_piety = 0.4
	elven_forebears_opinion = 10
	movement_speed = 0.1
	pursue_efficiency = 0.1
}

flag_of_the_diranhria_modifier = {
	monthly_piety = 0.5
	tolerance_advantage_mod = 4
}

ebonsteel_sword_of_bjarnrik_modifier = {
	monthly_dynasty_prestige = 0.04
	monthly_prestige = 0.2
	prowess = 7
	learning_per_prestige_level = 1
}

codex_navius_modifier = {
	monthly_prestige = 0.3
	naval_movement_speed_mult = 0.75
	embarkation_cost_mult = -0.2
	monthly_martial_lifestyle_xp_gain_mult = 0.15
}

moorsword_modifier = {
	prowess = 6
	monthly_prestige = 0.3
	wetlands_advantage = 4
	skirmishers_damage_mult = 0.1
	skirmishers_toughness_mult = 0.1
}

cuirass_of_the_khenaks_modifier = {
	monthly_prestige = 0.4
	negate_prowess_penalty_add = 6
	mountains_advantage = 4
	hard_casualty_modifier = -0.07
	general_opinion = 10
	spouse_opinion = 10
}

ladys_eagle_modifier = {
	monthly_prestige = 0.4
	forest_advantage = 6
	hard_casualty_modifier = -0.08
	enemy_hard_casualty_modifier = 0.08
	martial_per_stress_level = 1
}
