k_eaglecrest = {
	1000.1.1 = {
		change_development_level = 8
	}
	1010.4.2 = {
		holder = 56 #Warde Eaglecrest
	}
}

c_eaglecrest = {
	1000.1.1 = {
		change_development_level = 10
	}
}

c_lodan_crags = {
	1000.1.1 = {
		change_development_level = 6
	}
}

c_feycombe = {
	1000.1.1 = {
		change_development_level = 7
	}
}

c_south_floodmarsh = {
	1000.1.1 = {
		change_development_level = 6
	}
}

c_north_floodmarsh = {
	1000.1.1 = {
		change_development_level = 6
	}
}