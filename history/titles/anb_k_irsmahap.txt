k_irsmahap = {
	#3rd Dynasty 1-154
	1.1.1 = {
		holder = panoutid_0001
	}
	68.1.1 = {
		holder = panoutid_0002
	}
	105.1.1 = {
		holder = panoutid_0003
	}
	145.1.1 = {
		holder = panoutid_0004
	}
	#4th Dynasty 154-233
	154.1.1 = {
		holder = shenotid_0001
	}
	167.1.1 = {
		holder = shenotid_0002
	}
	188.1.1 = {
		holder = shenotid_0003
	}
	203.1.1 = {
		holder = shenotid_0004
	}
	#5th Dynasty 233-475
	233.1.1 = {
		holder = sokkanid_0001
	}
	254.1.1 = {
		holder = sokkanid_0002
	}
	287.1.1 = {
		holder = sokkanid_0003
	}
	316.1.1 = {
		holder = sokkanid_0004
	}
	367.1.1 = {
		holder = sokkanid_0005
	}
	393.1.1 = {
		holder = sokkanid_0006
	}
	463.1.1 = {
		holder = sokkanid_0007
	}
	#Aakhet
	475.1.1 = {
		holder = aakhet_the_bronze
	}
	#6th Dynasty 490-800
	490.1.1 = {
		holder = crodamis_0001
	}
	532.1.1 = {
		holder = crodamis_0002
	}
	581.1.1 = {
		holder = crodamis_0003
	}
	628.1.1 = {
		holder = crodamis_0004
	}
	662.1.1 = {
		holder = crodamis_0005
	}
	706.1.1 = {
		holder = crodamis_0006
	}
	757.1.1 = {
		holder = crodamis_0007
	}
	#7th Dynasty 800-1444
	800.1.1 = {
		holder = crodamos_0001
	}
	862.1.1 = {
		holder = crodamos_0002
	}
	904.1.1 = {
		holder = crodamos_0003
	}
	949.1.1 = {
		holder = crodamos_0004
	}
	1008.1.1 = {
		holder = crodamos_0005
	}
}