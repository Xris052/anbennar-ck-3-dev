﻿
# Silmuna

silmuna_0001 = {
	name = "Marion"
	dna = 1_marion_firstborn
	dynasty = dynasty_silmuna
	religion = cult_of_the_dame
	culture = damerian
	
	diplomacy = 13
	martial = 10
	stewardship = 4
	intrigue = 9
	learning = 2
	prowess = 13	# hes a good enough knight to fight sorcerer king or be selected
	
	trait = race_half_elf
	trait = education_martial_3
	trait = reckless
	trait = gregarious
	trait = ambitious
	trait = impatient
	trait = beauty_good_2
	trait = fecund
	trait = magical_affinity_2
	
	mother = dameris_0012
	father = 20
	
	1001.8.19 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
			set_sexuality = bisexual
			if = {
				limit = {
					has_game_rule = enable_marked_by_destiny
				}
				add_character_modifier = { modifier = anb_marked_by_destiny_modifier }
			}
		}
		give_nickname = nick_firstborn
	}

	1020.10.31 = {	#Battle of Trialmount
		effect = {
			set_relation_friend = character:36	# Ottrac the Wexonard
			set_relation_friend = character:34	# Clarimonde of Oldhaven
		}
	}
	1021.12.25 = {
		add_spouse = sorncost_0001
	}
	1021.10.3 = {
		effect = {
			set_artifact_rarity_famed = yes
			create_artifact = {
				name = silverbound_axe_of_dameria_name
				description = silverbound_axe_of_dameria_description
				type = pedestal
				visuals = axe_court
				wealth = scope:wealth
				quality = scope:quality
				history = {
					actor = character:dameris_0012
					recipeint = character:silmuna_0001
					type = inherited
					date = 1021.10.3
				}
				modifier = silverbound_axe_of_dameria_modifier
				save_scope_as = newly_created_artifact
			}
			scope:newly_created_artifact = {
				set_variable = { name = historical_unique_artifact value = yes }
			}
		}
	}
}
# Dameris

dameris_0001 = { # Rogec Dameris, Count of Damucen
	name = "Rogec"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	
	father = rubentis_0015
	
	756.2.25 = {
		birth = yes
	}
	
	810.10.24 = {
		death = "810.10.24"
	}
}

dameris_0002 = { # Lorran Dameris, Count of Damucen
	name = "Lorran"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	
	father = dameris_0001
	
	790.11.30 = {
		birth = yes
	}
	
	840.5.12 = {
		death = "840.5.12"
	}
}

dameris_0003 = { # Arrel Dameris, Count of Damucen
	name = "Arrel"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	
	father = dameris_0002
	
	815.1.31 = {
		birth = yes
	}
	
	870.10.26 = {
		death = "870.10.26"
	}
}

dameris_0004 = { # Aucanus Dameris, Count of Damucen
	name = "Aucanus"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	
	father = dameris_0003
	
	835.8.10 = {
		birth = yes
	}
	
	880.10.3 = {
		death = "880.10.3"
	}
}

dameris_0005 = { # Canrec "Moonborn" Dameris, King of Dameria and Carneter
	name = "Canrec"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = education_diplomacy_3
	trait = ambitious
	trait = generous
	trait = chaste
	trait = diplomat
	
	father = dameris_0004
	
	863.7.15 = {
		birth = yes
	}
	
	897.8.30 = {
		give_nickname = nick_moonborn
	}
	
	915.10.30 = {
		death = "915.10.30"
	}
}

dameris_0006 = { # Aucanus Dameris, King of Dameria and Carneter
	name = "Aucanus"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = education_stewardship_2
	trait = diligent
	trait = cynical
	trait = vengeful
	
	father = dameris_0005
	
	893.4.18 = {
		birth = yes
	}
	
	910.8.1 = {
		add_spouse = lorentis_0004
	}
	
	940.2.2 = {
		death = "955.10.2"
	}
}

dameris_0007 = { # Arrel Dameris, King of Dameria and Carneter
	name = "Arrel"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = twin
	
	father = dameris_0006
	mother = lorentis_0004
	
	912.3.24 = {
		birth = yes
	}
	
	955.10.2 = {
		death = {
			death_reason = death_ill
		}
	}
}

dameris_0008 = { # Rogec Dameris
	name = "Rogec"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = twin
	
	father = dameris_0006
	mother = lorentis_0004
	
	912.3.24 = {
		birth = yes
	}
	
	948.7.7 = {
		death = {
			death_reason = death_battle
		}
	}
}

dameris_0009 = { # Josern Dameris, second husband of Reanna II of Lorent
	name = "Josern"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = carnetori
	
	trait = race_human
	trait = education_learning_2
	trait = patient
	trait = temperate
	trait = content
	
	father = dameris_0008
	mother = damerian70072
	
	935.10.27 = {
		birth = yes
	}
	
	978.10.20 = {
		add_matrilineal_spouse = lorentis_0007
	}
	
	997.3.3 = {
		death = "997.3.3"
	}
}

dameris_0010 = { # Aucanna Dameris
	name = "Aucanna"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = education_diplomacy_2
	trait = content
	trait = generous
	trait = deceitful
	
	father = dameris_0008
	mother = damerian70072
	
	948.10.4 = {
		birth = yes
	}
	969.5.27 = {
		add_spouse = deruwris_0001
	}
	1002.11.27 = {
		death = yes
	}
}

dameris_0011 = { # Galien Dameris, King of Dameria and Carneter
	name = "Galien"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = education_learning_4
	trait = patient
	trait = zealous
	trait = forgiving
	trait = theologian
	trait = beauty_good_3
	
	father = dameris_0007
	mother = damerian70071
	
	930.6.8 = {
		birth = yes
	}
	
	980.12.1 = {
		death = "980.12.1"
	}
}

dameris_0012 = { # Auci Dameris, Queen of Dameria
	name = "Auci"
	dna = 13_auci
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = education_learning_4
	trait = compassionate
	trait = trusting
	trait = content
	trait = beauty_good_2
	trait = magical_affinity_2
	
	father = dameris_0011
	
	976.5.5 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}

	977.1.1 = {
		give_nickname = nick_eightborn
	}

	1021.10.3 = {
		death = "1021.10.3"
	}
}

dameris_0013 = { # Marven Dameris, King of Carneter
	name = "Marven"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = carnetori
	
	trait = education_martial_2
	trait = reckless
	trait = impatient
	trait = arrogant
	trait = brave
	trait = physique_good_3
	trait = race_human
	
	father = dameris_0011
	
	980.2.4 = {
		birth = yes
	}
	
	998.2.1 = {
		death = "998.2.1"
	}
}

dameris_0014 = { # Crege Dameris, King of Carneter
	name = "Crege"
	dna = 9_crege_dameris
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = carnetori
	
	diplomacy = 6
	martial = 10
	stewardship = 10
	intrigue = 4
	learning = 1
	prowess = 12 #Trained with Marion growing up, fought alongside him through Escann
	
	trait = race_human
	trait = education_stewardship_3
	trait = just
	trait = diligent
	trait = brave
	trait = physique_good_3
	
	father = dameris_0013
	
	998.4.28 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	
	1002.1.1 = {
		effect = {
			set_relation_best_friend = character:silmuna_0001	
		}
	}
	
	1018.3.2 = {
		add_spouse = 12
		effect = {
			set_relation_lover = character:12
		}
	}
}

dameris_0015 = { # Taliesin Dameris
	name = "Taliesin"
	dynasty_house = house_dameris
	religion = cult_of_the_dame
	culture = carnetori
	
	trait = race_human
	trait = pensive
	trait = physique_good_2
	
	father = dameris_0014
	mother = 12
	
	1018.11.14 = {
		birth = yes
	}
}

damerian_0001 = { # High Priestess Aria of the Highest Moon
	name = "Aria"
	# lowborn
	religion = cult_of_the_dame
	culture = damerian
	female = yes
	
	trait = race_human
	trait = education_learning_3
	trait = arbitrary
	trait = fickle
	trait = zealous
	
	961.8.1 = {
		birth = yes
	}
}

7000 = { # Westyn I "the Elder" of Wesdam
	name = "Westyn"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = honest
	trait = impatient
	trait = diligent
	trait = education_stewardship_2
	
	father = 7003
	mother = 7004
	
	986.12.17 = {
		birth = yes
	}
	
	1005.2.5 = {
		add_spouse = 7001
	}
	
	1018.4.13 = {
		death = {
			death_reason = death_duel
			killer = 50018 # Trutgard of Marllin
		}
	}
}

7001 = { # Amelie, wife of Westyn "the Elder"
	name = "Amelie"
	dynasty_house = house_roilsard
	religion = court_of_adean
	culture = roilsardi
	female = yes
	
	trait = race_human
	trait = gregarious
	trait = patient
	trait = content
	trait = education_diplomacy_1
	
	father = 504
	
	988.4.11 = {
		birth = yes
	}
	
	1005.2.5 = {
		add_spouse = 7000
	}
}

7002 = { # Westyn II "the Younger" of Wesdam
	name = "Westyn"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = shy
	trait = humble
	trait = chaste
	trait = education_learning_2
	
	father = 7000
	mother = 7001
	
	1005.11.2 = {
		birth = yes
	}
}

7003 = { # Ricard I of Wesdam
	name = "Ricard"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = wrathful
	trait = cynical
	trait = stubborn
	trait = education_martial_3
	
	father = damerian7009
	
	964.6.13 = {
		birth = yes
	}
	
	985.11.26 = {
		add_spouse = 7004
	}
	
	1014.7.24 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

7004 = { # Cora, wife of Ricard
	name = "Cora"
	dynasty = dynasty_gabalaire
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = humble
	trait = shy
	trait = forgiving
	trait = depressed_1
	trait = education_learning_2

	968.4.30 = {
		birth = yes
	}
	
	985.11.26 = {
		add_spouse = 7003
	}
	
	1021.12.31 = {
		employer = 7002
	}
}

damerian7005 = { # William I "the Westward" of Wesdam
	name = "William"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = diligent
	trait = brave
	trait = calm
	trait = education_martial_4
	trait = unyielding_defender
	trait = strategist
	trait = overseer
	
	861.3.12 = {
		birth = yes
	}
	
	898.5.2 = {
		give_nickname = nick_the_westward
	}
	
	915.12.20 = {
		death = "915.12.20"
	}
}

damerian7006 = { # Caylen of Wesdam
	name = "Caylen"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = temperate
	trait = generous
	trait = humble
	trait = education_stewardship_4
	trait = administrator
	
	father = damerian7005
	
	878.7.18 = {
		birth = yes
	}
	
	934.2.23 = {
		death = "934.2.23"
	}
}

damerian7007 = { # William II of Wesdam
	name = "William"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = fickle
	trait = gregarious
	trait = gluttonous
	trait = education_diplomacy_2
	
	father = damerian7006
	
	900.5.31 = {
		birth = yes
	}
	
	945.3.26 = {
		death = "945.3.26"
	}
}

damerian70071 = { # Moruith of Wesdam, wife of Arrel Dameris
	name = "Moruith"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = twin
	
	father = damerian7006
	
	908.7.27 = {
		birth = yes
	}
	
	970.6.20 = {
		death = "970.6.20"
	}
}

damerian70072 = { # Milian of Wesdam, wife of Rogec Dameris
	name = "Milian"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = twin
	
	father = damerian7006
	
	908.7.27 = {
		birth = yes
	}
	
	980.9.10 = {
		death = "980.9.10"
	}
}

damerian7008 = { # Devan of Wesdam
	name = "Devan"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = cynical
	trait = callous
	trait = stubborn
	trait = education_learning_1
	
	father = damerian7007
	
	919.1.18 = {
		birth = yes
	}
	
	974.3.25 = {
		death = "974.3.25"
	}
}

damerian7009 = { # William III of Wesdam
	name = "William"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = honest
	trait = diligent
	trait = forgiving
	trait = education_stewardship_2
	
	father = damerian7008
	
	943.10.6 = {
		birth = yes
	}
	
	990.11.25 = {
		death = {
			death_reason = death_consumption
		}
	}
}

damerian7010 = { # Petran, Trutgaud's father
	name = "Petran"
	dynasty = dynasty_wesdam
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = arbitrary
	trait = cynical
	trait = ambitious
	
	father = damerian7008
	
	950.4.2 = {
		birth = yes
	}
	
	994.6.17 = { # Treachery at Tomansford
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

damerian7011 = { # Daran Sil Cestir
	name = "Daran"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_martial_3
	trait = education_martial_prowess_3
	trait = reckless
	trait = brave
	trait = impatient
	trait = arbitrary
	
	father = damerian7039
	
	984.10.4 = {
		birth = yes
	}
	
	1002.7.4 = {
		add_spouse = damerian7015
	}
}

damerian7012 = { # Lain Sil Cestir, first chiled of Daran
	name = "Lain"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_diplomacy_2
	trait = temperate
	trait = compassionate
	trait = calm
	trait = one_legged
	
	father = damerian7011
	mother = damerian7015
	
	1004.1.8 = {
		birth = yes
	}
}

damerian7013 = { # Toman Sil Cestir, second child of Daran
	name = "Toman"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_intrigue_2
	trait = fickle
	trait = arrogant
	trait = callous
	
	father = damerian7011
	mother = damerian7015
	
	1007.5.7 = {
		birth = yes
	}
}

damerian7014 = { # Auci Sil Cestir, third child of Daran
	name = "Auci"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	female = yes
	
	trait = race_human
	trait = curious
	trait = trusting
	trait = patient
	disallow_random_traits = yes
	
	father = damerian7011
	mother = damerian7015
	
	1010.2.14 = {
		birth = yes
	}
}

damerian7015 = { # Dalya Cannleis, Daran's wife
	name = "Dayla"
	dynasty = dynasty_cannleis
	religion = cult_of_the_dame
	culture = damerian
	female = yes
	
	trait = race_human
	trait = education_stewardship_2
	trait = generous
	trait = ambitious
	trait = cynical
	
	father = damerian7016
	
	987.5.28 = {
		birth = yes
	}
	
	1002.7.4 = {
		add_spouse = damerian7011
	}
}

damerian7016 = { # Godric Cannleis, count of Cannleigh
	name = "Godric"
	dynasty = dynasty_cannleis
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = education_learning_1
	trait = content
	trait = lazy
	trait = craven
	
	957.2.2 = {
		birth = yes
	}
}

damerian7017 = { # Clarya Cannleis, sister of Dayla, died in childbirth
	name = "Clarya"
	dynasty = dynasty_cannleis
	religion = cult_of_the_dame
	culture = old_damerian
	female = yes
	
	trait = race_human
	trait = education_stewardship_1
	trait = greedy
	trait = trusting
	trait = callous
	
	father = damerian7016
	
	983.7.27 = {
		birth = yes
	}
	
	999.9.21 = {
		# marries some dude, maybe a count near
	}
	
	1002.9.3 = {
		death = {
			death_reason = death_childbirth
		}
	}
}

exwes_0001 = { # Dominic of Exwes, Duke of Exwes
	name = "Dominic"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = humble
	trait = zealous
	trait = open_terrain_expert
	
	959.6.23 = {
		birth = yes
	}
	
	994.2.9 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

exwes_0002 = { # Duke William of Exwes
	name = "William"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	
	trait = race_human
	trait = education_diplomacy_2
	trait = gluttonous
	trait = content
	trait = forgiving
	
	father = exwes_0001
	
	982.10.23 = {
		birth = yes
	}
	
	1000.9.13 = {
		add_spouse = damerian7019
	}
}

damerian7019 = { # Margery of Hookfield. wife of William of Exwes
	name = "Margery"
	dynasty = dynasty_hookfield
	religion = cult_of_the_dame
	culture = exwesser
	female = yes
	
	trait = race_human
	trait = education_learning_2
	trait = calm
	trait = chaste
	trait = compassionate
	
	984.7.17 = {
		birth = yes
	}
	
	1000.9.13 = {
		add_spouse = exwes_0002
	}
}

exwes_0003 = { # Albert of Exwes, heir of Exwes
	name = "Albert"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	
	trait = race_human
	trait = education_stewardship_2
	trait = diligent
	trait = gregarious
	trait = stubborn
	
	father = exwes_0002
	mother = damerian7019
	
	1002.11.15 = {
		birth = yes
	}
}

exwes_0004 = { # Lisbet of Exwes, daughter of William of Exwes
	name = "Lisbet"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	female = yes
	
	trait = race_human
	trait = education_learning_2
	trait = honest
	trait = patient
	trait = stubborn
	
	father = exwes_0002
	mother = damerian7019
	
	1004.4.20 = {
		birth = yes
	}
}

damerian7022 = { # Alena, lowborn who sired Albert of Exwes's kids
	name = "Alena"
	# lowborn
	religion = cult_of_the_dame
	culture = exwesser
	female = yes
	
	trait = race_human
	trait = education_diplomacy_1
	trait = gregarious
	trait = brave
	trait = stubborn
	
	1002.8.6 = {
		birth = yes
	}
	
	1020.9.30 = {
		death = {
			death_reason = death_childbirth
		}
	}
}

exwes_0005 = { # Arrel of Exwes, 1st son of Albert of Exwes
	name = "Arrel"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	
	trait = race_human
	trait = twin
	trait = legitimized_bastard
	
	father = exwes_0003
	mother = damerian7022
	
	1020.9.30 = {
		birth = yes
		set_relation_guardian = character:exwes_0004
	}
}

exwes_0006 = { # Adela of Exwes, 2nd daughter of Albert of Exwes
	name = "Adela"
	dynasty = dynasty_exwes
	religion = cult_of_the_dame
	culture = exwesser
	female = yes
	
	father = exwes_0003
	mother = damerian7022
	
	trait = race_human
	trait = twin
	trait = legitimized_bastard
	
	1020.9.30 = {
		birth = yes
		set_relation_guardian = character:exwes_0004
	}
}

damerian7030 = { 
	name = "Calas"
	dynasty = dynasty_dhaneir
	religion = "cult_of_the_dame"
	culture = "damerian"
	
	trait = education_diplomacy_3
	trait = just
	trait = gregarious	
	trait = content 
	trait = race_human
	

	969.01.03 = {
		birth = yes
	}
}

damerian7031 = { 
	name = "ReA_nna"
	dynasty = dynasty_dhaneir
	religion = "cult_of_the_dame"
	culture = "damerian"
	female = yes

	trait = education_diplomacy_2
	trait = zealous
	trait = compassionate	
	trait = diligent 
	trait = beauty_good_1
	trait = disinherited
	trait = race_half_elf 
	
	father = damerian7030
	mother = damerian7032

	1005.07.24 = {
		birth = yes
	}
}

damerian7032 = {# wife of Calas
	name = "Ariathen"
	religion = cult_of_the_dame
	culture = moon_elvish
	female = yes
	
	trait = race_elf
	trait = impatient
	trait = stubborn
	trait = brave
	trait = beauty_good_2
	trait = education_martial_2
	
	896.11.17 = {
		birth = yes
	}
	
	1005.7.4 = {
		add_spouse = damerian7030
	}
}

damerian7033 = { # count of Elensbridge 
	name = "Elecast"
	dynasty = dynasty_elentis
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = race_human
	trait = education_diplomacy_1
	trait = compassionate
	trait = trusting
	trait = fickle
	trait = infirm 
	
	954.9.19 = {
		birth = yes
	}
}

damerian7034 = { # heir to elensbridge
	name = "Hucbert"
	dynasty = dynasty_elentis
	religion = "cult_of_the_dame"
	culture = "old_damerian"

	martial = 10
	intrigue = 12
	
	trait = race_human
	trait = education_intrigue_2
	trait = callous
	trait = gregarious	
	trait = brave 
	trait = military_engineer
	trait = human_purist 
	
	father = damerian7033

	990.07.20 = {
		birth = yes
	}
}

damerian7035 = {# wife of hucbert
	name = "Castennia"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	female = yes
	
	trait = race_human
	trait = diligent
	trait = chaste
	trait = temperate
	trait = lifestyle_blademaster
	
	father = damerian7039
	
	992.9.28 = {
		birth = yes
	}
	
	1013.7.4 = {
		add_spouse = damerian7034
	}
}

damerian7036 = { 
	name = "Raynold"
	dynasty = dynasty_elentis
	religion = cult_of_the_dame
	culture = old_damerian
	
	trait = curious
	trait = race_human
	
	father = damerian7034
	mother = damerian7035
	
	1014.9.30 = {
		birth = yes
	}
}

damerian7037 = {
	name = "Coraline"
	dynasty = dynasty_elentis
	religion = cult_of_the_dame
	culture = damerian
	female = yes
	diplomacy = 11
	trait = race_human
	trait = fickle 
	trait = just
	trait = vengeful 
	
	father = damerian7033
	
	996.2.13 = {
		birth = yes
	}
	
	1014.2.7 = {
		effect = {
			set_relation_rival = character:damerian7034
		}
	}
}

damerian7038 = { 
	name = "Daylon"
	dynasty = dynasty_elentis
	religion = "cult_of_the_dame"
	culture = "old_damerian"
	learning = 15
	trait = education_learning_2
	trait = content
	trait = humble	
	trait = compassionate 
	trait = lifestyle_physician
	trait = race_human
	
	father = damerian7033

	991.11.13 = {
		birth = yes
	}
}

damerian7039 = { 
	name = "Tristan"
	dynasty = dynasty_cestir
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_martial_1
	trait = stubborn
	trait = calm
	trait = arbitrary
	

	951.03.13 = {
		birth = yes
	}
	 
	1003.12.8 = {
		death = 1003.12.8
	}
}

natharis0001 = { # Lenedius Natharis, count of Tirufeld
	name = "Lenedius"
	dynasty = dynasty_natharis
	religion = cult_of_the_dame
	culture = damerian
	
	trait = race_human
	trait = education_learning_3
	trait = arbitrary
	trait = stubborn
	trait = zealous
	trait = physique_good_2
	trait = depressed_genetic
	trait = loyal
	trait = reclusive
	
	959.4.23 = {
		birth = yes
	}
	
	978.3.21 = {
		add_spouse = natharis0002
	}
}

natharis0002 = { # Emmalinda, wife of Lenedius
	name = "Emmalinda"
	# lowborn
	religion = cult_of_esmaryal
	culture = old_esmari
	female = yes
	
	trait = race_human
	trait = education_diplomacy_1
	trait = greedy
	trait = compassionate
	trait = shy
	trait = fecund
	trait = depressed_genetic
	
	957.12.19 = {
		birth = yes
	}
	
	978.3.21 = {
		add_spouse = natharis0001
	}
	
	1014.10.3 = {
		death = {
			death_reason = death_depressed
		}
	}
}

natharis0003 = 	{ # Son 1 of them
	name = "Lorran"
	dynasty = dynasty_natharis
	religion = cult_of_the_dame
	culture = damerian
	mother = natharis0002
	father = natharis0001
	
	trait = race_human
	trait = education_martial_2
	trait = brave
	trait = cynical
	trait = humble
	trait = reckless
	
	980.7.5 = {
		birth = yes
	}
	
	1004.6.16 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

natharis0004 = 	{ # Son 2 of them
	name = "Geraint"
	dynasty = dynasty_natharis
	religion = cult_of_the_dame
	culture = damerian
	mother = natharis0002
	father = natharis0001
	
	trait = race_human
	trait = education_martial_3
	trait = callous
	trait = vengeful
	trait = temperate
	trait = forder
	
	983.3.24 = {
		birth = yes
	}
	
	1014.5.14 = {
		death = {
			death_reason = death_battle_of_morban_flats
		}
	}
}	

natharis0005 = 	{ # Son 3 of them
	name = "Emmeran"
	dynasty = dynasty_natharis
	religion = cult_of_the_dame
	culture = damerian
	mother = natharis0002
	father = natharis0001
	
	trait = race_human
	trait = education_diplomacy_2
	trait = generous
	trait = brave
	trait = honest
	
	989.7.22 = {
		birth = yes
	}
	
	1014.2.16 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

ilvan_0001 = { # Count of Ilvandet
	name = "Lucius"
	dna = lucius_ilvan
	dynasty_house = house_ilvan
	religion = cult_of_the_dame
	culture = carnetori
	mother = roilsard_0001
	father = deruwris_0002
	
	trait = race_human
	trait = education_stewardship_3
	trait = just
	trait = generous
	trait = content
	trait = physique_good_1
	
	990.9.30 = {
		birth = yes
	}
	1012.5.9 = {
		add_spouse = woodslover0001 #Imariel
	}
}

ilvan_0002 = { # Son 1
	name = "Thal"
	dynasty_house = house_ilvan
	religion = cult_of_the_dame
	culture = carnetori
	mother = woodslover0001
	father = ilvan_0001
	
	trait = race_half_elf
	trait = pensive
	trait = magical_affinity_2
	trait = physique_good_1
	
	1013.2.14 = {
		birth = yes
	}
}

ilvan_0003 = { # Son 2
	name = "Marven"
	dynasty_house = house_ilvan
	religion = cult_of_the_dame
	culture = carnetori
	mother = woodslover0001
	father = ilvan_0001
	
	trait = race_half_elf
	trait = magical_affinity_1
	
	1021.10.15 = {
		birth = yes
	}
}

deruwris_0001 = { # Grandpa of Lucius
	name = "Sulgen"
	dynasty = dynasty_deruwris
	religion = cult_of_the_dame
	culture = carnetori
	
	trait = race_human
	trait = education_martial_2
	trait = vengeful
	trait = stubborn
	trait = calm	
	
	950.6.23 = {
		birth = yes
	}
	969.5.27 = {
		add_spouse = dameris_0010
	}
	994.1.14 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

deruwris_0002 = { # Father of Lucius
	name = "Marven"
	dynasty = dynasty_deruwris
	religion = cult_of_the_dame
	culture = carnetori
	mother = dameris_0010
	father = deruwris_0001
	
	trait = race_human
	trait = education_martial_2
	trait = honest
	trait = generous
	trait = impatient	
	
	971.8.28 = {
		birth = yes
	}
	989.3.27 = {
		add_spouse = roilsard_0001
		}
	1013.10.3 = {
		death = {
			death_reason = death_killed_war_of_sorcerer_king
		}
	}
}

deruwris_0003 = { # Aunt of Lucius
	name = "Moruith"
	dynasty = dynasty_deruwris
	religion = cult_of_the_dame
	culture = carnetori
	female = yes
	mother = dameris_0010
	father = deruwris_0001
	
	trait = race_human
	trait = education_learning_3
	trait = patient
	trait = eccentric
	trait = temperate
	trait = devoted
	trait = magical_affinity_1
	
	984.5.9 = {
		birth = yes
	}
}