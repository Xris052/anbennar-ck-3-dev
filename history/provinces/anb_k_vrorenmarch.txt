#k_vrorenmarch
##d_vrorenwall
###c_bal_vroren
740 = {		#Bal Vroren

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_bal_vroren_01
		special_building = castanorian_citadel_bal_vroren_01
	}
}
735 = {		#Seawall

    # Misc
    holding = city_holding

    # History
}
2628 = {

    # Misc
    holding = none

    # History

}
2629 = {

    # Misc
    holding = none

    # History

}
736 = {		#Isle Vroren

    # Misc
    holding = none

    # History
}

###c_mammoth_hall
737 = {		#Mammoth Hall

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2626 = {

    # Misc
    holding = city_holding

    # History

}
2627 = {

    # Misc
    holding = church_holding

    # History

}

###c_gulleton
2624 = {	#Gulleton

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
738 = {

    # Misc
    holding = city_holding

    # History

}
2625 = {

    # Misc
    holding = none

    # History

}

###c_serpentswall
739 = {		#Serpentswall

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2623 = {

    # Misc
    holding = none

    # History

}

##d_cedesck
###c_cedevik
717 = {		#Cedevik

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2636 = {

    # Misc
    holding = city_holding

    # History

}
2637 = {

    # Misc
    holding = church_holding

    # History

}
2638 = {

    # Misc
    holding = none

    # History

}
2639 = {

    # Misc
    holding = none

    # History

}

###c_coldwood
719 = {		#Coldwood

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2640 = {

    # Misc
    holding = none

    # History

}

###c_rivsby
2630 = {	#Rivsby

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
718 = {

    # Misc
    holding = city_holding

    # History

}
2631 = {

    # Misc
    holding = church_holding

    # History

}
2632 = {

    # Misc
    holding = none

    # History

}
2633 = {

    # Misc
    holding = none

    # History

}

###c_vertwic
720 = {		#Vertwic

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2634 = {

    # Misc
    holding = none

    # History

}
2635 = {

    # Misc
    holding = none

    # History

}

##d_sondaar
###c_sondaar
714 = {		#Sondaar

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = city_holding

    # History
}
2641 = {

    # Misc
    holding = castle_holding

    # History

}
2642 = {

    # Misc
    holding = none

    # History

}

###c_eskerborg
715 = {		#Eskerborg

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = city_holding

    # History
}
2643 = {

    # Misc
    holding = castle_holding

    # History

}
2644 = {

    # Misc
    holding = none

    # History

}

##d_wudhal
###c_wudhal
711 = {		#Wudhal

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2645 = {

    # Misc
    holding = city_holding

    # History

}
2646 = {

    # Misc
    holding = none

    # History

}

###c_tannusvale
716 = {		#Tannusvale

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2647 = {

    # Misc
    holding = none

    # History

}
2648 = {

    # Misc
    holding = none

    # History

}

##d_east_chillsbay
###c_esald
701 = {		#Esald

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = city_holding

    # History
}
2652 = {

    # Misc
    holding = city_holding

    # History

}
2653 = {

    # Misc
    holding = church_holding

    # History

}
2654 = {

    # Misc
    holding = none

    # History

}

###c_estshore
712 = {		#Estmarck

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2651 = {

    # Misc
    holding = none

    # History

}

###c_whitwic
709 = {		#Whitwic

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = city_holding

    # History
}
2649 = {

    # Misc
    holding = church_holding

    # History

}
2650 = {

    # Misc
    holding = none

    # History

}

###c_chillpoint
713 = {		#Chillpoint

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2655 = {

    # Misc
    holding = none

    # History

}
2656 = {

    # Misc
    holding = church_holding

    # History

}

###c_three_giants
707 = {		#Estjotun

    # Misc
    culture = white_reachman
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2657 = {

    # Misc
    holding = none

    # History

}
2658 = {

    # Misc
    holding = none

    # History

}

##d_ebonmarck
###c_ebonham
745 = {		#Ebonham

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2617 = {

    # Misc
    holding = city_holding

    # History

}
2618 = {

    # Misc
    holding = none

    # History

}

###c_acenvror
748 = {		#Acenvror

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2615 = {

    # Misc
    holding = church_holding

    # History

}
2616 = {

    # Misc
    holding = none

    # History

}

###c_fortroad
744 = {		#Fortroad

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2619 = {

    # Misc
    holding = none

    # History

}

###c_giantswood
741 = {		#Giantswood

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2622 = {

    # Misc
    holding = none

    # History

}

###c_giantsfort
742 = {		#Giantsfort

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2620 = {

    # Misc
    holding = city_holding

    # History

}
2621 = {

    # Misc
    holding = none

    # History

}

##d_tencfor
###c_tencfor
730 = {		#Tencfor

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2611 = {

    # Misc
    holding = city_holding

    # History

}
2612 = {

    # Misc
    holding = none

    # History

}

###c_gerudswatch
732 = {		#Gerudswatch

    # Misc
    culture = black_castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2613 = {

    # Misc
    holding = church_holding

    # History

}
2614 = {

    # Misc
    holding = none

    # History

}
