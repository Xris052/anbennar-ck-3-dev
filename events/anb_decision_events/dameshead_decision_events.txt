﻿namespace = anb_dameshead_events

# Consolidate Arbaran
anb_dameshead_events.0001 = {
	type = character_event
	title = anb_dameshead_events.0001.t
	desc = anb_dameshead_events.0001.desc
	theme = realm
	left_portrait = {
		character = root
		animation = personality_honorable
	}
	
	immediate = {
		consolidate_arbaran_decision_effect = yes
	}

	option = { 
		name = anb_dameshead_events.0001.a
	}
}

anb_dameshead_events.0002 = {
	type = character_event
	title = anb_dameshead_events.0002.t
	desc = anb_dameshead_events.0002.desc
	theme = war
	right_portrait = {
		character = scope:arbaran_former
		animation = personality_zealous
	}
	
	immediate = {
		show_as_tooltip = { consolidate_arbaran_decision_effect = yes }
	}

	option = {
		name = {
			trigger = {
				NOR = {
					has_title = title:k_gawed
					has_title = title:k_damescrown
				}
			}
			text = anb_dameshead_events.0002.a
		}
		name = {
			trigger = {
				OR = {
					has_title = title:k_gawed
					has_title = title:k_damescrown
				}
			}
			text = anb_dameshead_events.0002.b
		}
	}
}

# Encourage Arbarani
anb_dameshead_events.0003 = {
	type = character_event
	title = anb_dameshead_events.0003.t
	desc = anb_dameshead_events.0003.desc
	theme = realm

	immediate = {
		create_arbarani_effect = yes
	}

	option = { 
		name = anb_dameshead_events.0003.a
	}
}

anb_dameshead_events.0004 = {
	type = character_event
	title = anb_dameshead_events.0004.t
	desc = anb_dameshead_events.0004.desc
	theme = realm
	left_portrait = root
	right_portrait = {
		character = scope:arbaran_former
		animation = personality_zealous
	}
	
	immediate = {
		show_as_tooltip = { consolidate_arbaran_decision_effect = yes }
	}

	# Conver to Arbarani
	option = {
		name = anb_dameshead_events.0004.a
		
		embrace_arbarani_effect = yes
		
		ai_chance = {
			base = 100
		}
	}
	
	# Don't convert
	option = {
		name = anb_dameshead_events.0004.b

		trigger = {
			OR = {
				is_ai = no
				culture = culture:crownsman
			}
		}
		
		ai_chance = {
			base = 100
		}
	}
}
