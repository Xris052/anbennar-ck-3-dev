﻿# These assets are then used in the genes (game/common/genes/..) at the bottom of the file. 

	#set_tags = "no_hair"
	#set_tags = "no_beard"
	#set_tags = "crown"
	#set_tags = "cap"
	#set_tags = "hat"
	#set_tags = "hood_beard"
	#set_tags = "cap_02"
	#set_tags = "helmet"
	#set_tags = "headdress"
	#set_tags = "cap,cap_02"
	#set_tags = "aventail"


################
#####Western####
############################

###open helmets

male_headgear_western_war_helmet_01 = {	
	set_tags = "helmet"
	set_tags = "no_hair"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_western_war_helmet_01_entity 	 }		
}
male_headgear_western_war_helmet_08 = {	
	set_tags = "helmet"
	set_tags = "no_hair"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_western_war_helmet_08_entity 	 }		
}

###close helms

male_headgear_western_war_helmet_02 = {	
	set_tags = "helmet"
	set_tags = "no_hair"	
	set_tags = "no_beard"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_western_war_helmet_02_entity 	 }		
}

male_headgear_western_war_helmet_03 = {	
	set_tags = "helmet"
	set_tags = "no_hair"	
	set_tags = "no_beard"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_western_war_helmet_03_entity 	 }		
}

male_headgear_western_war_helmet_04 = {	
	set_tags = "helmet"
	set_tags = "no_hair"	
	set_tags = "no_beard"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_western_war_helmet_04_entity 	 }		
}

##kettle helm

male_headgear_western_war_helmet_05 = {	
	set_tags = "helmet"
	set_tags = "no_hair"	
	set_tags = "hood_beard"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_western_war_helmet_05_entity 	 }		
}

male_headgear_western_war_helmet_06 = {	
	set_tags = "helmet"
	set_tags = "no_hair"	
	set_tags = "hood_beard"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_western_war_helmet_06_entity 	 }		
}

male_headgear_western_war_helmet_07 = {	
	set_tags = "helmet"
	set_tags = "no_hair"	
	set_tags = "hood_beard"	
	entity = { required_tags = ""			shared_pose_entity = head		entity = male_headgear_western_war_helmet_07_entity 	 }		
}




















